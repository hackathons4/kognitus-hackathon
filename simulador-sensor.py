## Gerador de dados
import random
import math

def x_const():
    R, rad, x, dx, y, z = 0.25, 1.0, 0.0, 0.5, 0.0, 0.0
    f = open("dados1.txt", "w")

    while(x < 200):
        x += dx

        y = R*math.cos(x)
        z = R*math.sin(x)

        x = float("{0:.2f}".format(x))
        y = float("{0:.2f}".format(y))
        z = float("{0:.2f}".format(z))

        st=str(x)+" "+str(y)+" "+str(z)+" "+str(rad)+"\n"

        f.write(st)

    f.close()

def x_varia():
    R, rad, x, y, z = 0.25, 1.0, 0.0, 0.0, 0.0
    f = open("dados2"+".txt", "w")

    while(x < 200):
        if(x >= 45 and x <= 150):
            dx = 0.5
        else:
            dx = 0.1
        x += dx
        R = math.cos(x)
        R = R*10
        y = R*math.cos(x)
        z = R*math.sin(x)

        x = float("{0:.2f}".format(x))
        y = float("{0:.2f}".format(y))
        z = float("{0:.2f}".format(z))

        st=str(x)+" "+str(y)+" "+str(z)+" "+str(rad)+"\n"

        f.write(st)

    f.close()

def rad_varia():
    R, rad, x, dx, y, z = 0.25, 1.0, 0.0, 0.5, 0.0, 0.0
    f = open("dados3"+".txt", "w")

    while(x < 200):
        if(x == 35):
            rad = 0.5

        x += dx

        y = R*math.cos(x)
        z = R*math.sin(x)

        x = float("{0:.2f}".format(x))
        y = float("{0:.2f}".format(y))
        z = float("{0:.2f}".format(z))

        st=str(x)+" "+str(y)+" "+str(z)+" "+str(rad)+"\n"

        f.write(st)

    f.close()


x_const()
x_varia()
rad_varia()
